create schema if not exists joinusdb;

create table if not exists joinusdb.JOINUS (
                                                   JOINUS_NOM varchar(40) not null,
                                                   JOINUS_PRENOM varchar(40) not null,
                                                   JOINUS_EMAIL varchar(40) not null,
                                                   JOINUS_DATEDENAISSANCE varchar(40) not null,
                                                   JOINUS_DELEGATION varchar(40) not null,
                                                   JOINUS_GOUVERNAURAT varchar(40) not null,
                                                   JOINUS_ADDRESS varchar(40) not null,
                                                   JOINUS_NIVEAUEDUCATIF varchar(40) not null,
                                                   JOINUS_VOUSETES varchar(40) not null,
                                                   JOINUS_ETABLISSEMENT varchar(40) not null,
                                                   JOINUS_SPECIALITE varchar(40) not null,
                                                   JOINUS_EMPLOI varchar(40) not null,
                                                   JOINUS_NUMTELEPHONE integer null,
                                                   JOINUS_SCOUT boolean,
                                                   constraint PK_FEEDBACK_NOM primary key (JOINUS_NOM)
);
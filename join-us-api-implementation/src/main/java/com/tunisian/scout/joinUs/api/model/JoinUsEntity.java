package com.tunisian.scout.joinUs.api.model;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Data
@Entity
@Table( name = "joinus",schema = "joinusdb")
public class JoinUsEntity {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String nom;
    private String prenom;
    private String email;
    private String dateDeNaissance;
    private String delegation;
    private String gouvernaurat;
    private String address;
    private String niveauEducatif;
    private String vousEtes;
    private String etablissement;
    private String specialite;
    private String emploi;
    private int numTelephone;
    private boolean scout;

    public JoinUsEntity(String nom, String prenom, String email, String dateDeNaissance, String delegation, String gouvernaurat, String address, String niveauEducatif, String vousEtes, String etablissement, String specialite, String emploi, int numTelephone, boolean scout) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.dateDeNaissance = dateDeNaissance;
        this.delegation = delegation;
        this.gouvernaurat = gouvernaurat;
        this.address = address;
        this.niveauEducatif = niveauEducatif;
        this.vousEtes = vousEtes;
        this.etablissement = etablissement;
        this.specialite = specialite;
        this.emploi = emploi;
        this.numTelephone = numTelephone;
        this.scout = scout;
    }

    public JoinUsEntity() {

    }
}

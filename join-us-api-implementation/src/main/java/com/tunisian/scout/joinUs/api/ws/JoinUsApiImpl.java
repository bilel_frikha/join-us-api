package com.tunisian.scout.joinUs.api.ws;

import com.demo.join.api.v1.JoinUsApi;
import com.demo.join.api.v1.dto.JoinUs;
import com.tunisian.scout.joinUs.api.business.JoinUsBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.core.Response;

@Controller
public class JoinUsApiImpl implements JoinUsApi {

    @Autowired
    JoinUsBusiness joinUsBusiness;

    @Override
    public Response deleteMember(String memberId) {
        return null;
    }

    @Override
    public Response sendMember ( JoinUs member ) {

        joinUsBusiness.sendMail(member);
        return Response.ok().build();
    }

}

package com.tunisian.scout.joinUs.api.ws.config;

import com.demo.join.api.v1.JoinUsApi;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("joinusapi/v1")
public class JoinUsWSConfig extends ResourceConfig {
    public JoinUsWSConfig() { registerEndpoints();}
    private void registerEndpoints() { register(JoinUsApi.class);}
}

package com.tunisian.scout.joinUs.api.manager.impl;

import com.demo.join.api.v1.dto.JoinUs;
import com.tunisian.scout.joinUs.api.manager.JoinUsManager;
import com.tunisian.scout.joinUs.api.mapper.JoinUsMapper;
import com.tunisian.scout.joinUs.api.model.JoinUsEntity;
import com.tunisian.scout.joinUs.api.repository.JoinUsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class JoinUsManagerimpl implements JoinUsManager {
        @Autowired
        JoinUsRepository joinUsRepository;
        @Autowired
        JoinUsMapper joinUsMapper;
        @Autowired
        private JavaMailSender emailSender;

        private static final String NOREPLY_ADDRESS = "bilel90fr@gmail.com";

        @Override
        public void sendMail(JoinUs member) {

            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setFrom(NOREPLY_ADDRESS);
                message.setTo("bilel.frikha@yahoo.fr");
                message.setSubject("New Member wants to join us: his name is " + member.getNom());
                message.setText(member.getPrenom());

                emailSender.send(message);
            } catch (MailException exception) {
                exception.printStackTrace();
            }
        }

        @Override
        public void saveMember(JoinUs member) {
            JoinUsEntity jointUsEntity= new JoinUsEntity();
            jointUsEntity = joinUsMapper.joinUsToJoinUsEntity(member);
            joinUsRepository.save(jointUsEntity);

        }
}

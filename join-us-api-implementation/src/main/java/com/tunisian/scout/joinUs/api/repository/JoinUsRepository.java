package com.tunisian.scout.joinUs.api.repository;

import com.tunisian.scout.joinUs.api.model.JoinUsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JoinUsRepository extends JpaRepository<JoinUsEntity, String> {

}

package com.tunisian.scout.joinUs.api.business.impl;


import com.demo.join.api.v1.dto.JoinUs;
import com.tunisian.scout.joinUs.api.business.JoinUsBusiness;
import com.tunisian.scout.joinUs.api.manager.JoinUsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JoinUsBusinessimpl implements JoinUsBusiness {
    @Autowired
    JoinUsManager joinUsManager;

    @Override
    public void sendMail(JoinUs member) {
        joinUsManager.sendMail(member);
        joinUsManager.saveMember(member);
    }


}

package com.tunisian.scout.joinUs.api.manager;

import com.demo.join.api.v1.dto.JoinUs;

public interface JoinUsManager {
    public void sendMail(JoinUs member);
    public void saveMember (JoinUs member);
}

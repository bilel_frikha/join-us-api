package com.tunisian.scout.joinUs.api.mapper;
import com.demo.join.api.v1.dto.JoinUs;
import com.tunisian.scout.joinUs.api.model.JoinUsEntity;
import org.springframework.stereotype.Component;

@Component
public class JoinUsMapper {

    public JoinUsEntity joinUsToJoinUsEntity(JoinUs member){
        if (member==null){
            return null;
        }
        JoinUsEntity joinUsEntity = new JoinUsEntity();
        joinUsEntity.setNom(member.getNom());
        joinUsEntity.setPrenom(member.getPrenom());
        joinUsEntity.setEmail(member.getEmail());
        joinUsEntity.setDateDeNaissance(member.getDateDeNaissance());
        joinUsEntity.setDelegation(member.getDelegation());
        joinUsEntity.setGouvernaurat(member.getGouvernaurat());
        joinUsEntity.setNiveauEducatif(member.getNiveauEducatif());
        joinUsEntity.setVousEtes(member.getVousEtes());
        joinUsEntity.setEtablissement(member.getEtablissement());
        joinUsEntity.setSpecialite(member.getSpecialite());
        joinUsEntity.setEmploi(member.getEmploi());
        joinUsEntity.setNumTelephone(member.getNumTelephone());
        joinUsEntity.setScout(member.isScout());
        return joinUsEntity;
    }

    public JoinUs joinUsEntityToJoinUs (JoinUsEntity joinUsEntity){
        if ( joinUsEntity == null ){
            return null;
        }
        JoinUs member = new JoinUs();
        member.setNom(joinUsEntity.getNom());
        member.setPrenom(joinUsEntity.getPrenom());
        member.setEmail(joinUsEntity.getEmail());
        member.setDateDeNaissance(joinUsEntity.getDateDeNaissance());
        member.setDelegation(joinUsEntity.getDelegation());
        member.setGouvernaurat(joinUsEntity.getGouvernaurat());
        member.setNiveauEducatif(joinUsEntity.getNiveauEducatif());
        member.setVousEtes(joinUsEntity.getVousEtes());
        member.setEtablissement(joinUsEntity.getEtablissement());
        member.setSpecialite(joinUsEntity.getSpecialite());
        member.setEmploi(joinUsEntity.getEmploi());
        member.setNumTelephone(joinUsEntity.getNumTelephone());
        member.setScout(joinUsEntity.isScout());
      return member;
    }

}



